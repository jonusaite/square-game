Destroy colored squares game.

Copyright (c) 2017 Aistė Jonušaitė

Licensed under [MIT License](https://opensource.org/licenses/mit-license.html)