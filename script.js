$(document).ready(function() {
	// Create starting squares.
	var transquares = [];
	for (var i = 0; i < 49; i++) {
		transquares.push("<div class='transquare'></div>");
	};
	$('.game').append(transquares.join(""));
	// 
	$('.start').one('click', function() {
		var pause = false;
		var square = 0;
		var points = 0;
		// Squares array, which will be used for assigning random square.
		var uniqueRandoms = [];
		for (var n = 1; n < 50; n++) {
			uniqueRandoms.push(n);
		};
		// Fucntion for interval between squares emerging.
		function sleep(time) {
			return new Promise((resolve) => setTimeout(resolve, time));
		};
		// Squares emerging function.
		function run() {
			sleep((125 - points) * 10).then(() => {
				// When clicking "stop" it is true and stops the game.
				if (pause) {
					return;
				};
				// Choose random square that will be colored.
				var index = Math.floor(Math.random() * uniqueRandoms.length);
				var a = uniqueRandoms[index];
				uniqueRandoms.splice(index, 1);
				// Giving lifes to squared from 1 to 5.
				var life = Math.floor((Math.random() * 5) + 1);
				var nth = $('.transquare:nth-child(' + a + ')');
				$(nth).removeClass('transquare').addClass('square');
				$(nth).attr('data-lifes', life);
				$(nth).attr('data-reward', life); // The same life count, but doesn't count down. For counting points.
				// Adding to squares count.
				square++;
				$('.squares_left').text("Squares left " + square);
				// When all squares are colored, game over.
				if(square >= 49) {
					$('.game').prepend("<div class='end'>Game over!</div>");
					return;
				};
				// When 100 points are reached, game is won.
				if (points >= 100) {
					$('.game').prepend("<div class='end'>You Won!</div>");
					return;
				};
				run();
			});
		};
		run();
		// Squares destroying function.
		function destroy() {
			if (points < 100) {
				// Estimate lifes count for clicked square.
				var lifesLeft = $(this).attr('data-lifes');
				// If it is the last life, square disappears, reward are calculated and added to points and square number is returned to the array for possible squares to color again.
				if (lifesLeft == 1) {
					var reward = $(this).attr('data-reward');
					$(this).removeClass('square').addClass('transquare');
					$(this).removeAttr('data-lifes');
					points += Number(reward);
					var b = $(this).index() + 1; 
					uniqueRandoms.splice(1, 0, b);
					square -= 1;
					$('.points').text("Points: " + points);
					$('.squares_left').text("Squares left: " + square);
				};
				// If life count is more than 1, then clicking on the squares takes 1 life from it.
				if (lifesLeft > 1) {
					$(this).attr('data-lifes', lifesLeft - 1);
				};
			};
		};
		$('.game').on('click', '.square', destroy);
		// After clicking "stop", square emerging stops and player can't destroy squares.
		function gamepause() {
			pause = true;
			$('.game').off('click');
			$('.pause').off('click');
			$('.continue').on('click', gamecontinue);
		};
		// Clicking "continue" game continues.
		function gamecontinue() {
			pause = false;
			run();
			$('.game').on('click', '.square', destroy);
			$('.pause').on('click', gamepause);
			$('.continue').off('click');
		};
		$('.pause').on('click', gamepause);
	});
	$('.restart').on('click', function() {
		location.reload();
	});
});
